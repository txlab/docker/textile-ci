import { execute } from '@textile/buck-util'

async function run(): Promise<void> {
  const api = process.env['TEXTILE_API'] || ''
  const key: string = process.env['TEXTILE_API_KEY'] || ''
  const secret: string = process.env['TEXTILE_API_SECRET'] || ''
  const thread: string = process.env['TEXTILE_THREAD'] || ''
  const bucketName: string = process.env['TEXTILE_BUCKET'] || ''
  const remove: string = process.env['TEXTILE_REMOVE'] || 'false'

  const pattern = process.env['TEXTILE_PUSH_PATTERN'] || '**/*'
  const dir = process.env['TEXTILE_PUSH_DIR'] || ''
  const home = process.env['TEXTILE_HOME'] || './'

  try {
    const result = await execute(api, key, secret, thread, bucketName, remove, pattern, dir, home)
    result.forEach((value, key) => console.log(key, value))
  } catch (error) {
    console.error(error)
    process.exit(1)
  }
}

run()