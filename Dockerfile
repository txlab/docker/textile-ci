FROM node:14.21-alpine as builder

WORKDIR /build
COPY package.json yarn.lock tsconfig.json ./

RUN yarn --frozen-lockfile

COPY /src/ ./src/

RUN yarn build

############
## RUNNER ##
############
FROM node:14.21-alpine as runner

WORKDIR /scripts

COPY package.json yarn.lock ./
RUN yarn --frozen-lockfile

COPY --from=builder /build/dist/*.js ./
# make the scripts runnable
RUN chmod +x ./*.js \
    && sed -i '1i #!/usr/bin/env node' ./*.js

CMD [ "/scripts/push.js" ]
